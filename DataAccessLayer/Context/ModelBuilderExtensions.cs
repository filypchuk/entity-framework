﻿using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataAccessLayer.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasOne(project => project.User)
                .WithMany(user => user.Projects)
                .HasForeignKey(project => project.AuthorId);
            modelBuilder.Entity<Project>()
                .HasOne(project => project.Team)
                .WithMany(team => team.Projects)
                .HasForeignKey(project => project.TeamId);

            modelBuilder.Entity<TaskEntity>()
                .HasOne(task => task.User)
                .WithMany(user => user.Tasks)
                .HasForeignKey(task => task.PerformerId);
            modelBuilder.Entity<TaskEntity>()
                .HasOne(task => task.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(task => task.ProjectId);

            modelBuilder.Entity<User>()
              .HasOne(u => u.Team)
              .WithMany(t => t.Users)
              .HasForeignKey(u => u.TeamId);


            modelBuilder.Entity<Project>()
                .HasOne(project => project.User)
                .WithMany(user => user.Projects)
                .OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<Project>()
                .HasOne(project => project.Team)
                .WithMany(t => t.Projects)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Projects)
                .WithOne(p => p.Team)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Users)
                .WithOne(p => p.Team)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<TaskEntity>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TaskEntity>()
                .HasOne(t => t.User)
                .WithMany(u => u.Tasks)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Users)
                .OnDelete(DeleteBehavior.SetNull);

        }
        public static void FluentApi(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(p => p.Name).IsRequired().HasMaxLength(128);

            modelBuilder.Entity<TaskEntity>().Property(t => t.Name).IsRequired().HasMaxLength(128);
            modelBuilder.Entity<TaskEntity>().Property(t => t.State).IsRequired();
            modelBuilder.Entity<TaskEntity>().Property(t => t.ProjectId).IsRequired();

            modelBuilder.Entity<Team>().Property(t => t.Name).IsRequired().HasMaxLength(64);

            modelBuilder.Entity<User>().Property(t => t.FirstName).IsRequired().HasMaxLength(32);
            modelBuilder.Entity<User>().Property(t => t.LastName).IsRequired().HasMaxLength(32);
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var projects = ProjectsData();
            var tasks = TasksData();
            var team = TeamsData();
            var user = UsersData();

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<TaskEntity>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(team);
            modelBuilder.Entity<User>().HasData(user);
        }
        private static ICollection<Project> ProjectsData()
        {
            return ReadAndDeserializeData<Project>("Projects.json");
        }
        private static ICollection<TaskEntity> TasksData()
        {
            return ReadAndDeserializeData<TaskEntity>("Tasks.json");
        }
        private static ICollection<Team> TeamsData()
        {
            return ReadAndDeserializeData<Team>("Teams.json");
        }
        private static ICollection<User> UsersData()
        {
            return ReadAndDeserializeData<User>("Users.json");
        }
        private static ICollection<T> ReadAndDeserializeData<T>(string fileName)
        {
            string str = "";
            using (StreamReader sr = new StreamReader($"../DataAccessLayer/JsonData/{fileName}"))
            {
                str = sr.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<ICollection<T>>(str);
        }
    }
}
