﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;


namespace DataAccessLayer.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private protected readonly ProjectDbContext _context;

        public TeamRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Team> GetAll()
        {
            return _context.Teams
                .Include(t=>t.Users);
        }

        public Team GetById(int id)
        {
            return _context.Teams.Find(id);
        }

        public Team Create(Team entity)
        {
            _context.Teams.Add(entity);
            return entity;
        }
        public void Update(Team entity)
        {
            _context.Teams.Update(entity);
        }

        public void Delete(int id)
        {
            Team entity = _context.Teams.Find(id);
            if (entity != null)
                _context.Teams.Remove(entity);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
