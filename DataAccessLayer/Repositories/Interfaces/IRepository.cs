﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        public IEnumerable<T> GetAll();
        public T GetById(int id);
        public T Create(T entity);
        public void Update(T entity);
        public void Delete(int id);
        void Save();
    }
}
