﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces
{
   public interface ITeamRepository : IRepository<Team>    {  }
}
