﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface ITaskRepository : IRepository<TaskEntity> 
    {
    }
}
