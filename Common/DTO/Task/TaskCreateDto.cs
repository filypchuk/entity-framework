﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.Task
{
    public sealed class TaskCreateDto
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        [Required]
        public int ProjectId { get; set; }
        public int? PerformerId { get; set; }
    }
}
