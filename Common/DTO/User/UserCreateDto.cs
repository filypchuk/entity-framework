﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTO.User
{
    public sealed class UserCreateDto
    {
        [Required]
        [MaxLength(32)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(32)]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public int? TeamId { get; set; }
    }
}
