﻿using Common.DTO.Team;
using ConsoleClient.ClientService;
using ConsoleClient.Helpers;
using System;

namespace ConsoleClient.View
{
    public class TeamView
    {
        private readonly TeamClientService _service;
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        private readonly CheckOutput checkOutput;
        public TeamView()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
            checkOutput = new CheckOutput();
            _service = new TeamClientService();
        }
        public void AllTeams()
        {
            Console.Clear();
            print("Get all teams", Color.Yellow);
            var listDto = _service.GetAll();
            if (!checkOutput.EmptyList(listDto))
            {
                foreach (var dto in listDto)
                {
                    print(dto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public void TeamById()
        {
            Console.Clear();
            print("Get team by id", Color.Yellow);
            print("Enter team id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = _service.GetById(id);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public void CreateTeam()
        {
            Console.Clear();
            print("Create team", Color.Yellow);
            TeamCreateDto createDto = new TeamCreateDto();
            print("Enter name", Color.Green);
            createDto.Name = checkInput.CheckingString();

            var dto = _service.Create(createDto);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public void UpdateTeam()
        {
            Console.Clear();
            print("Update team", Color.Yellow);
            print("Enter team id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = _service.GetById(id);
            if (!checkOutput.NoContent(dto))
            {
                print(dto.ToString(), Color.Yellow);
                TeamDto updateDto = new TeamDto();
                updateDto.Id = dto.Id;
                print("Enter new name", Color.Green);
                updateDto.Name = checkInput.CheckingString();
                updateDto.CreatedAt = dto.CreatedAt;
                var responseDto = _service.Update(updateDto);
                print("Updated dto", Color.Green);
                if (!checkOutput.NoContent(responseDto))
                {
                    print(responseDto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public void Delete()
        {
            Console.Clear();
            print("Delete team", Color.Yellow);
            print("Enter team id", Color.Green);
            int id = checkInput.CheckingInt();
            _service.Delete(id);
            printWaitingForEnter();
        }
        public void TeamAndUsers()
        {
            Console.Clear();
            print("Get a list (id, team name and user list) from a collection of teams over 10 years old, \n" +
                "sorted by date of user registration in descending order, and grouped by team.", Color.None);
            var list = _service.TeamAndUsers();
            if (!checkOutput.EmptyList(list))
            {
                foreach (var dto in list)
                {
                    print($"Id -- {dto.Id} \t Name -- {dto.Name}", Color.Green);
                    if (!checkOutput.EmptyList(list))
                    {
                        print("Users", Color.Yellow);
                        foreach (var user in dto.Users)
                        {
                            print(user.ToString(), Color.Yellow);
                        }
                    }
                    print("-------------------------------", Color.Blue);
                }
            }
            printWaitingForEnter();
        }
    }
}
