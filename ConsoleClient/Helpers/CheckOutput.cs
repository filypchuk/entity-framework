﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient.Helpers
{
    public class CheckOutput
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        public CheckOutput()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
        }
        public bool NoContent(Object ob)
        {
            if (ob == null)
            {
                print("No Content", Color.Red);
                return true;
            }
            else
                return false;
        }
        public bool EmptyList<T>(IEnumerable<T> ob)
        {
            if (ob.Count() == 0)
            {
                print("Empty List", Color.Red);
                return true;
            }
            else
                return false;
        }
    }
}
