﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;

namespace ConsoleClient.ConsoleMenu
{
    public class TeamMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly TeamView view;
        public TeamMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            view = new TeamView();
        }
        private void DisplayTeamMenu()
        {
            Console.Clear();
            print("Click button 1-6 to select \n", Color.Red);
            print("1---Get all Team \n", Color.Green);
            print("2---Get Team by id \n", Color.Green);
            print("3---Create Team \n", Color.Blue);
            print("4---Update Team \n", Color.Yellow);
            print("5---Delete Team \n", Color.Red);
            print("6---Get a list (id, team name and user list) from a collection of teams over 10 years old \n", Color.Green);
            print("\n   Click Esc to back menu", Color.Red);
        }
        public void StartTeamMenu()
        {
            DisplayTeamMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        view.AllTeams();
                        break;
                    case ConsoleKey.D2:
                        view.TeamById();
                        break;
                    case ConsoleKey.D3:
                        view.CreateTeam();
                        break;
                    case ConsoleKey.D4:
                        view.UpdateTeam();
                        break;
                    case ConsoleKey.D5:
                        view.Delete();
                        break;
                    case ConsoleKey.D6:
                        view.TeamAndUsers();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        DisplayTeamMenu();
                        break;
                }
                DisplayTeamMenu();
            }
        }
    }
}
