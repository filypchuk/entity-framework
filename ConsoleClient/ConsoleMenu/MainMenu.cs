﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;

namespace ConsoleClient.ConsoleMenu
{
    class MainMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly ProjectMenu projectMenu;
        private readonly TaskMenu taskMenu;
        private readonly TeamMenu teamMenu;
        private readonly UserMenu userMenu;
        public MainMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            projectMenu = new ProjectMenu();
            taskMenu = new TaskMenu();
            teamMenu = new TeamMenu();
            userMenu = new UserMenu();
        }
        public void Start()
        {
            DisplayMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        projectMenu.StartProjectMenu();
                        break;
                    case ConsoleKey.D2:
                        taskMenu.StartTaskMenu();
                        break;
                    case ConsoleKey.D3:
                        teamMenu.StartTeamMenu();
                        break;
                    case ConsoleKey.D4:
                        userMenu.StartUserMenu();
                        break;
                    default:
                        DisplayMenu();
                        break;
                }
                DisplayMenu();
            }
        }
        private void DisplayMenu()
        {
            Console.Clear();
            print("Click button 1-4 to select \n", Color.Red);
            print("1---Projects ", Color.Green);
            print("2---Tasks ", Color.Green);
            print("3---Teams ", Color.Green);
            print("4---Users ", Color.Green);
        }       
    }
}
