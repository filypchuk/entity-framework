﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        public ProjectService(IProjectRepository projRepository, IMapper mapper) : base(mapper)
        {
            _projectRepository = projRepository;
        }
        public IEnumerable<ProjectDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDto>>(_projectRepository.GetAll());
        }
        public ProjectDto GetById(int id)
        {
            return _mapper.Map<ProjectDto>(_projectRepository.GetById(id));
        }
        public ProjectDto Create(ProjectCreateDto dto)
        {
            var newTeam = _mapper.Map<Project>(dto);
            newTeam.CreatedAt = DateTime.Now;
            var created = _projectRepository.Create(newTeam);
            _projectRepository.Save();
            return _mapper.Map<ProjectDto>(created);
        }
        public ProjectDto Update(ProjectDto dto)
        {
            var entityUpdate = _mapper.Map<Project>(dto);
            _projectRepository.Update(entityUpdate);
            _projectRepository.Save();
            return _mapper.Map<ProjectDto>(_projectRepository.GetById(dto.Id));
        }
        public void Delete(int id)
        {
            _projectRepository.Delete(id);
            _projectRepository.Save();
        }
        public IEnumerable<ProjectAndCountTasksDto> TasksInProjectByUser(int userId)
        {
            var projectsAndCount = _projectRepository.GetAll()
                     .Where(p => p.AuthorId == userId)
                     .Select(x =>
                     new ProjectAndCountTasks
                     {
                         Key = x,
                         Value = x.Tasks.Count
                     });
            return _mapper.Map<IEnumerable<ProjectAndCountTasksDto>>(projectsAndCount);
        }
        public IEnumerable<ProjectAndTwoTasksDto> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            var projectsAndTasks = _projectRepository.GetAll()
                .Select(project => 
                new ProjectAndTwoTasks
                {
                    Project = project,
                    TheLongestTask = project.Tasks.OrderBy(task => task.Description.Length).LastOrDefault(),
                    TheShortestTask = project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault(),
                    CountUsersInTeam = project.Team?.Users.Where(u => project.Description.Length > 20 || (project.Tasks.Count) < 3).Count() ?? 0
                });
            return _mapper.Map<IEnumerable<ProjectAndTwoTasksDto>>(projectsAndTasks);
        }
    }
}
