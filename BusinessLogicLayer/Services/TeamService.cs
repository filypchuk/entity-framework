﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        public TeamService(ITeamRepository teamRepository, IMapper mapper) : base(mapper) 
        {
            _teamRepository = teamRepository;
        }
        public IEnumerable<TeamDto> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(_teamRepository.GetAll());
        }

        public TeamDto GetById(int id)
        {
            return _mapper.Map<TeamDto>(_teamRepository.GetById(id));
        }
        public TeamDto Create(TeamCreateDto dto)
        {
            var newTeam = _mapper.Map<Team>(dto);
            newTeam.CreatedAt = DateTime.Now;
            var created =_teamRepository.Create(newTeam);
            _teamRepository.Save();
            return _mapper.Map<TeamDto>(created);
        }
        public TeamDto Update(TeamDto dto)
        {
            var entityUpdate = _mapper.Map<Team>(dto);
            _teamRepository.Update(entityUpdate);
            _teamRepository.Save();
            return _mapper.Map<TeamDto>(_teamRepository.GetById(dto.Id));
        }
        public void Delete(int id)
        {
            _teamRepository.Delete(id);
            _teamRepository.Save();
        }
        public IEnumerable<TeamAndUsersDto> TeamAndUsersOlderTenYears()
        {
            var teamAndUsers = _teamRepository.GetAll()
                .Select(team=>
                        new TeamAndUsers
                        {
                            Id = team.Id,
                            Name = team.Name,
                            Users = team.Users.Where(u => u.Birthday.Year <= DateTime.Now.Year - 10)
                                    .OrderByDescending(u => u.RegisteredAt)
                        })
                .Where(team => team.Users.Any(u => u.Birthday.Year <= DateTime.Now.Year - 10));

            return _mapper.Map<IEnumerable<TeamAndUsersDto>>(teamAndUsers);
        }
    }
}
