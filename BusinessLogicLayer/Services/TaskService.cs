﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly ITaskRepository _repository;
        public TaskService(ITaskRepository repository, IMapper mapper) : base( mapper) 
        {
            _repository = repository;
        }
        public IEnumerable<TaskDto> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(_repository.GetAll());
        }

        public TaskDto GetById(int id)
        {
            return _mapper.Map<TaskDto>(_repository.GetById(id));
        }
        public TaskDto Create(TaskCreateDto dto)
        {
            var newTask = _mapper.Map<TaskEntity>(dto);
            newTask.CreatedAt = DateTime.Now;
            var created = _repository.Create(newTask);
            _repository.Save();
            return _mapper.Map<TaskDto>(created);
        }

        public TaskDto Update(TaskDto dto)
        {
            var entityUpdate = _mapper.Map<TaskEntity>(dto);
            _repository.Update(entityUpdate);
            _repository.Save();
            return _mapper.Map<TaskDto>(_repository.GetById(dto.Id));
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
            _repository.Save();
        }
        public IEnumerable<TaskDto> TasksByUser(int userId)
        {
            var tasks = _repository.GetAll()
                .Where(task => task.PerformerId == userId && task.Name.Length < 45);

            return _mapper.Map<IEnumerable<TaskDto>>(tasks);
        }
        public IEnumerable<TasksFinishedByUserDto> TasksFinishedByUser(int userId)
        {
            return _repository.GetAll()
                .Where(task => task.PerformerId == userId && task.FinishedAt.Year == DateTime.Now.Year)
                .Select(t => new TasksFinishedByUserDto { Id = t.Id, Name = t.Name });
        }
    }
}
