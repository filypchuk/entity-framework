﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class UserByAlphabetAndTasks
    {
        public User User { get; set; }
        public IEnumerable<TaskEntity> Tasks { get; set; }
    }
}
