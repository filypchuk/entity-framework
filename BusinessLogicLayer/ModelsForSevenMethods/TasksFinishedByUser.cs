﻿namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class TasksFinishedByUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
