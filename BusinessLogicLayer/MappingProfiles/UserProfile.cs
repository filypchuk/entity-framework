﻿using AutoMapper;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using DataAccessLayer.Entities;
using BusinessLogicLayer.ModelsForSevenMethods;

namespace BusinessLogicLayer.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<UserCreateDto, User>();
            CreateMap<UserByAlphabetAndTasks, UserByAlphabetAndTasksDto>();
            CreateMap<UsersProjectTask, UsersProjectTaskDto>();
        }
    }
}
