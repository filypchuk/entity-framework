﻿using AutoMapper;
using Common.DTO.Task;
using DataAccessLayer.Entities;

namespace BusinessLogicLayer.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDto>();
            CreateMap<TaskDto, TaskEntity>();
            CreateMap<TaskCreateDto, TaskEntity>(); 
        }
    }
}